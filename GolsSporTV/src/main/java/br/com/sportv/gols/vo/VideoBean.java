package br.com.sportv.gols.vo;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;
import android.text.Html;
import android.util.Log;

import com.mobsandgeeks.adapters.InstantText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import br.com.sportv.gols.utils.Util;
import br.com.sportv.gols.R;


public class VideoBean implements Parcelable {
    private static final long serialVersionUID = -4124243480593543128L;
    public int id = Integer.MIN_VALUE;
    public int idGloboVideos = Integer.MIN_VALUE;
    public String titulo = null;
    public String descricao = null;
    public int idPartida;
    public String duracao = null;
    public GregorianCalendar data = new GregorianCalendar();
    public String midia = null;
    public String editoria = null;

    public VideoBean(JSONObject jo) throws JSONException {

        id = jo.getInt("id");
        idGloboVideos = jo.getInt("id_globo_videos");
        if (Build.VERSION.SDK_INT >= 11) {
            titulo = Html.fromHtml(jo.getString("titulo")).toString();
        } else {
            titulo = jo.getString("titulo");
        }
        if (Build.VERSION.SDK_INT >= 11) {
            descricao = Html.fromHtml(jo.getString("descricao")).toString();
        } else {
            descricao = jo.getString("descricao");
        }

        if (jo.has("partida") && !jo.isNull("partida")) {
            idPartida = jo.getInt("partida");
        }

        SimpleDateFormat dataTime = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        if (jo.has("data")) {
            try {
                Date tempData = dataTime.parse(jo.getString("data"));
                data.setTime(tempData);
            } catch (Exception e) {
                Log.e("SporTV", "erro na conversão da data", e);
            }
        }

        duracao = jo.getString("duracao");

        if (jo.has("url") && (!jo.get("url").equals(null))) {
            midia = jo.getString("url");
        } else if (jo.has("midias") && (!jo.get("midias").equals(null))) {
            JSONArray jMidias = jo.getJSONArray("midias");
            for (int i = 0; i < jMidias.length(); i++) {
                JSONObject jMidia = jMidias.getJSONObject(i);
                if (jMidia.getString("tipo").equalsIgnoreCase("M4")) {
                    midia = jMidia.getString("url");
                }
            }
        }
    }

    public VideoBean(int id, String titulo, String descricao, String duracao, GregorianCalendar data) {
        this.id = id;
        this.titulo = titulo;
        this.descricao = descricao;
        this.duracao = duracao;
        this.data = data;
    }

    public VideoBean(int id, String titulo, String descricao, String duracao, GregorianCalendar data, String midia) {
        this.id = id;
        this.titulo = titulo;
        this.descricao = descricao;
        this.duracao = duracao;
        this.data = data;
        this.midia = midia;
    }

    /**
     * Retorna a imagem globovideos para o formato 320x200
     *
     * @return a imagem globovideos para o formato 320x200
     */
    public String getThumb() {
        return "http://s01.video.glbimg.com/640x360/" + idGloboVideos + ".jpg";
    }

    /**
     * Retorna a data em formato String
     *
     * @return a data em formato String
     */
    @InstantText(viewId = R.id.textViewDataVideo)
    public String getDataStr() {
        return Util.calendarToString(data);
    }

    @InstantText(viewId = R.id.textViewTituloVideo)
    public String getTitulo() {
        return titulo;
    }

    @InstantText(viewId = R.id.textViewDuracaoVideo)
    public String getDuracao() {
        return duracao;
    }

    @InstantText(viewId = R.id.textViewCampeonatoVideo)
    public String getEditoria() {
        return editoria;
    }

    protected VideoBean(Parcel in) {
        id = in.readInt();
        idGloboVideos = in.readInt();
        titulo = in.readString();
        descricao = in.readString();
        idPartida = in.readInt();
        duracao = in.readString();
        data = (GregorianCalendar) in.readValue(null);
        midia = in.readString();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(idGloboVideos);
        dest.writeString(titulo);
        dest.writeString(descricao);
        dest.writeInt(idPartida);
        dest.writeString(duracao);
        dest.writeValue(data);
        dest.writeString(midia);
    }

    public static final Parcelable.Creator<VideoBean> CREATOR = new Parcelable.Creator<VideoBean>() {
        public VideoBean createFromParcel(Parcel in) {
            return new VideoBean(in);
        }

        public VideoBean[] newArray(int size) {
            return new VideoBean[size];
        }
    };
}