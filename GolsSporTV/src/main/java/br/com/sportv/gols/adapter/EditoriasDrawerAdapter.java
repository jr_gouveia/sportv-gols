package br.com.sportv.gols.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import br.com.sportv.gols.vo.EditoriaBean;


public class EditoriasDrawerAdapter extends ArrayAdapter<EditoriaBean> {

    private ArrayList<EditoriaBean> items;
    private Context context;
    private Activity atividade;
    private View v;

    public int getCount() {
        return items.size();
    }

    public EditoriasDrawerAdapter (Activity atividade, int textViewResourceId,
                                   ArrayList<EditoriaBean> items) {
        super(atividade.getApplicationContext(), textViewResourceId);
        this.items = items;
        this.context = atividade.getApplicationContext();
        this.atividade = atividade;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(android.R.layout.simple_list_item_1, null);
        }

        EditoriaBean c = items.get(position);

        if (c != null) {
            TextView text = (TextView)v.findViewById(android.R.id.text1);
            text.setText(c.nome);

        }

        return v;
    }

    @Override
    public EditoriaBean getItem(int i) {

        return items.get(i);
    }
}
