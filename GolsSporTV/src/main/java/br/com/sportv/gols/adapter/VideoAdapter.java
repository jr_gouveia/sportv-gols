package br.com.sportv.gols.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import br.com.sportv.gols.R;
import br.com.sportv.gols.vo.VideoBean;

public class VideoAdapter extends ArrayAdapter<VideoBean> {
    protected ArrayList<VideoBean> items;
    protected Context context;
    protected View v;
    protected String chapeu;

    public VideoAdapter(Context context, int textViewResourceId, ArrayList<VideoBean> items, String chapeu) {
        super(context, textViewResourceId, items);
        this.items = items;
        this.context = context;
        this.chapeu = chapeu;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        v = convertView;
        Log.d("SporTV", "getView " + position + " " + convertView);
        LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        if (v == null) {
            v = vi.inflate(R.layout.videos_listitem, null);
        }

        VideoBean e = (VideoBean) items.get(position);
        if (e != null) {
            TextView txtTitulo = (TextView) v.findViewById(R.id.textViewTituloVideo);
            txtTitulo.setText(e.titulo);

            TextView txtData = (TextView) v.findViewById(R.id.textViewDataVideo);
            txtData.setText(String.valueOf(e.getDataStr()));

            if (chapeu != null) {
                TextView txtEditoria = (TextView) v.findViewById(R.id.textViewCampeonatoVideo);
                txtEditoria.setText(chapeu.toUpperCase());
            }

            TextView txtDuracao = (TextView) v.findViewById(R.id.textViewDuracaoVideo);
            txtDuracao.setText(e.getDuracao());


            ImageView imgThumb = (ImageView) v.findViewById(R.id.imageViewThumbVideo);
            Picasso.with(context).load(e.getThumb()).into(imgThumb);
            Log.d("SporTV", "load url image " + e.getThumb());
        }

        return v;
    }
}
