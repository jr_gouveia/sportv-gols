package br.com.sportv.gols.sharedprefs;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

public class AppPrefs {
	
	private static String prefsKey;
	public final static String TOKEN = "325";
	
	//controles de atualizacao de versao
	
	
	public AppPrefs(Activity atividade) {
		prefsKey = "APPGOLSSPORTV";
	}
	
	public AppPrefs(Context context) {
		prefsKey = "APPGOLSSPORTV";
	}
	
	public void putString(Context context, String chave, String valor) {
		SharedPreferences prefs = context.getSharedPreferences(prefsKey, Context.MODE_PRIVATE);
		if (prefs != null) {
			Editor edit = prefs.edit();
			if (edit != null) {
				edit.putString(chave, valor);
				edit.commit();
				Log.d("PREFS", "putString("+chave+" : "+valor+") do widget "+prefsKey);
			}
		}
	}

	public void putInt(Context context, String chave, int valor) {
		SharedPreferences prefs = context.getSharedPreferences(prefsKey, Context.MODE_PRIVATE);
		if (prefs != null) {
			Editor edit = prefs.edit();
			if (edit != null) {
				edit.putInt(chave, valor);
				edit.commit();
				Log.d("PREFS", "putInt("+chave+" : "+valor+") do widget "+prefsKey);
			}
		}
	}
	
	public void putFloat(Context context, String chave, Float valor) {
		SharedPreferences prefs = context.getSharedPreferences(prefsKey, Context.MODE_PRIVATE);
		if (prefs != null) {
			Editor edit = prefs.edit();
			if (edit != null) {
				edit.putFloat(chave, valor);
				edit.commit();
				Log.d("PREFS", "putFloat("+chave+" : "+valor+") do widget "+prefsKey);
			}
		}
	}
	
	public void putBoolean(Context context, String chave, boolean valor) {
		SharedPreferences prefs = context.getSharedPreferences(prefsKey, Context.MODE_PRIVATE);
		if (prefs != null) {
			Editor edit = prefs.edit();
			if (edit != null) {
				edit.putBoolean(chave, valor);
				edit.commit();
				Log.d("PREFS", "putBoolean("+chave+" : "+valor+") do widget "+prefsKey);
			}
		}
	}
	
	public String getString(Context context, String chave) {
		SharedPreferences prefs = context.getSharedPreferences(prefsKey, Context.MODE_PRIVATE);
		if (prefs != null) {
			Log.d("PREFS", "getString("+chave+" : "+prefs.getString(chave, "INFORMA")+") do widget "+prefsKey);
			return prefs.getString(chave, null);
		}
		return null;
	}
	
	public int getInt(Context context, String chave) {
		SharedPreferences prefs = context.getSharedPreferences(prefsKey, Context.MODE_PRIVATE);
		if (prefs != null) {
			Log.d("PREFS", "getInt("+chave+" : "+prefs.getInt(chave, 0)+") do widget "+prefsKey);
			return prefs.getInt(chave, 0);
		}
		return -1;
	}
	
	public Float getFloat(Context context, String chave) {
		SharedPreferences prefs = context.getSharedPreferences(prefsKey, Context.MODE_PRIVATE);
		if (prefs != null) {
			Log.d("PREFS", "getFloat("+chave+" : "+prefs.getFloat(chave, 0)+") do widget "+prefsKey);
			return prefs.getFloat(chave, Float.valueOf("16"));
		}
		return Float.valueOf("16");
	}
	
	public boolean getBoolean(Context context, String chave) {
		SharedPreferences prefs = context.getSharedPreferences(prefsKey, Context.MODE_PRIVATE);
		if (prefs != null) {
			Log.d("PREFS", "getBoolean("+chave+" : "+prefs.getBoolean(chave, false)+") do widget "+prefsKey);

			return prefs.getBoolean(chave, false);
		}
		return false;
	}
	
	public boolean contains(Context context, String chave) {
		SharedPreferences prefs = context.getSharedPreferences(prefsKey, Context.MODE_PRIVATE);
		if (prefs != null) {
			//Log.d("PREFS", "contains("+chave+" : "+prefs.getBoolean(chave, false)+") do widget "+prefsKey);
			return prefs.contains(chave);
		}
		return false;
	}
	
	public boolean remove(Context context, String chave) {
		SharedPreferences prefs = context.getSharedPreferences(prefsKey, Context.MODE_PRIVATE);
		if (prefs != null) {
			Editor edit = prefs.edit();
			if (edit != null) {
				edit.remove(chave);
				return edit.commit();
			}
		}
		return false;
	}
	
	
	public static void delete(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(prefsKey, Context.MODE_PRIVATE);
		if (prefs != null) {
			prefs.edit().clear();
			prefs.edit().commit();
			
		}
	}

}
