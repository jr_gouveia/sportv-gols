package br.com.sportv.gols;


import android.content.res.Configuration;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;

import com.loopj.android.http.JsonHttpResponseHandler;
import com.mobsandgeeks.adapters.InstantAdapter;
import com.mobsandgeeks.adapters.ViewHandler;

import org.apache.http.client.HttpResponseException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import br.com.sportv.gols.fragments.VideosFragment;
import br.com.sportv.gols.http.ApiRestClient;
import br.com.sportv.gols.utils.Util;
import br.com.sportv.gols.vo.EditoriaBean;
import uk.co.senab.actionbarpulltorefresh.extras.actionbarcompat.PullToRefreshAttacher;


public class HomeActivity extends ActionBarActivity {

    public static final String SELECIONE_UM_CAMPEONATO = "Selecione uma opção";
    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    ListView mEditoriasListView;
    ArrayList<EditoriaBean> mEditoriasList = new ArrayList<EditoriaBean>();
    String mTitulo;
    private PullToRefreshAttacher mPullToRefreshAttacher;
    private final static String FRAGMENT_VIDEOS_ID = "fragmentsVideosId";
    boolean fragmentAttached = false;
    int positionSelected;
    LinearLayout mLeftDrawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        mLeftDrawer = (LinearLayout) findViewById(R.id.left_drawer);


        mTitulo = getString(R.string.app_name);

        // Create a PullToRefreshAttacher instance
        mPullToRefreshAttacher = PullToRefreshAttacher.get(this);


        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer icon to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description */
                R.string.drawer_close  /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                setTitle(mTitulo);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                setTitle(SELECIONE_UM_CAMPEONATO);
            }
        };

        if (savedInstanceState != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            VideosFragment mFragment = (VideosFragment) fragmentManager.findFragmentByTag(FRAGMENT_VIDEOS_ID);
            if (mFragment != null) {
                ft.attach(mFragment).commit();
                fragmentAttached = true;
            }
            if (savedInstanceState.containsKey("mTitulo") && savedInstanceState.getString("mTitulo") != null) {
                mTitulo = savedInstanceState.getString("mTitulo");
            }

            if (savedInstanceState.containsKey("positionSelected")) {
                positionSelected = savedInstanceState.getInt("positionSelected");
            }

            if (savedInstanceState.containsKey("listaEditorias")) {
                mEditoriasList = savedInstanceState.getParcelableArrayList("listaEditorias");
                drawMenu();
            }
        } else {

//            VideosFragment fragment = new VideosFragment();
//            Bundle args = new Bundle();
//            fragment.setArguments(args);
//            FragmentManager fragmentManager = getSupportFragmentManager();
//            fragmentManager.beginTransaction()
//                    .replace(R.id.content_frame, fragment, FRAGMENT_VIDEOS_ID)
//                    .commit();

            callServer();
        }
        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(android.R.color.white));

        setTitle(mTitulo);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            VideosFragment mFragment = (VideosFragment) fragmentManager.findFragmentByTag(FRAGMENT_VIDEOS_ID);
            if (mFragment != null) {
                ft.attach(mFragment).commit();
                fragmentAttached = true;
            }
            if (savedInstanceState.containsKey("mTitulo")) {
                mTitulo = savedInstanceState.getString("mTitulo");
                setTitle(mTitulo);
            }

            if (savedInstanceState.containsKey("positionSelected")) {
                positionSelected = savedInstanceState.getInt("positionSelected");
            }

            if (savedInstanceState.containsKey("listaEditorias")) {
                mEditoriasList = savedInstanceState.getParcelableArrayList("listaEditorias");
                drawMenu();
            }
        }
    }

    private void callServer() {
        ApiRestClient api = new ApiRestClient(getApplicationContext());
        api.getEditorias(new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(JSONObject editoriasJson) {
                Log.d("SporTV", "RETORNO " + editoriasJson.toString());

                try {
                    mEditoriasList = new ArrayList<EditoriaBean>();
                    EditoriaBean edGeral = new EditoriaBean();
                    edGeral.id = -1;
                    edGeral.nome = getString(R.string.app_name);
                    mEditoriasList.add(edGeral);
                    if (editoriasJson.has("count")) {
                        if (editoriasJson.getInt("count") > 0) {
                            if (editoriasJson.has("results")) {
                                JSONArray jResults = editoriasJson.getJSONArray("results");
                                for (int i = 0; i < jResults.length(); i++) {
                                    JSONObject jItem = jResults.getJSONObject(i);
                                    mEditoriasList.add(new EditoriaBean(jItem));
                                }
                            }
                        }
                    }

                    drawMenu();
                } catch (JSONException e) {
                    //    BugSenseHandler.sendExceptionMessage(TAG,
                    //            "Criação do objeto MercadoBean", e);
                }
                Log.d("SporTV", "RETORNO " + mEditoriasList.size());
            }

            @Override
            public void onSuccess(int i, String s) {
                super.onSuccess(i, s);
                Log.d("SporTV", "RETORNO 1");
            }

            @Override
            public void onSuccess(int i, JSONObject jsonObject) {
                super.onSuccess(i, jsonObject);
                Log.d("SporTV", "RETORNO 2");
            }

            @Override
            public void onSuccess(int i, JSONArray jsonArray) {
                super.onSuccess(i, jsonArray);
                Log.d("SporTV", "RETORNO 3");
            }

            @Override
            public void onStart() {
                super.onStart();
                Log.d("SporTV", "Start");
            }

            @Override
            public void onFailure(Throwable throwable) {
                super.onFailure(throwable);
                Log.d("SporTV", "fail");
            }

            @Override
            public void onFailure(Throwable e, String strError) {
                Log.d("SporTV", "RETORNO " + strError, e);
                //BugSenseHandler.sendExceptionMessage(TAG,
                //        "Erro na leitura do serviço do status do mercado",
                //        (Exception) e);
                //drawView();
                super.onFailure(e, strError);
            }

            @Override
            public void onFailure(Throwable throwable, JSONObject jsonObject) {
                super.onFailure(throwable, jsonObject);
                Log.d("SporTV", "RETORNO 4");
                Log.d("SporTV", "RETORNO " + jsonObject + " - " + ((HttpResponseException) throwable).getStatusCode(), throwable);
                if (throwable instanceof HttpResponseException) {
                    if (((HttpResponseException) throwable).getStatusCode() == 403) {

                    }
                }

            }

            @Override
            public void onFailure(Throwable throwable, JSONArray jsonArray) {
                super.onFailure(throwable, jsonArray);

                Log.d("SporTV", "RETORNO 5");
            }

            @Override
            public void onSuccess(String s) {
                super.onSuccess(s);
                Log.d("SporTV", "RETORNO 6");

            }

            @Override
            public void onSuccess(JSONArray jsonArray) {
                super.onSuccess(jsonArray);
                Log.d("SporTV", "RETORNO 7");

            }

        });
    }

    private void drawMenu() {
        if (mEditoriasList.size() > 0) {

            InstantAdapter<EditoriaBean> mAdapter = new InstantAdapter<EditoriaBean>(
                    this, R.layout.menu_listitem, EditoriaBean.class,
                    mEditoriasList);

            mAdapter.setViewHandler(R.id.menu_root, new ViewHandler<EditoriaBean>() {

                /**
                 * Allows you to make changes to an associated View by supplying the adapter, the View's
                 * parent, an instance of the data associated with the position and the position itself.
                 *
                 * @param adapter  The adapter to which this {@link com.mobsandgeeks.adapters.ViewHandler} is assigned to.
                 * @param parent   Parent for the View that is being handled. Usually the custom layout
                 *                 instance for individual views and a {@link android.widget.ListView} or a {@link android.widget.GridView}
                 *                 for the layout.
                 * @param view     The {@link android.view.View} that has to be handled.
                 * @param instance The instance that is associated with the position.
                 * @param position Position of the item within the adapter's data set.
                 */
                @Override
                public void handleView(ListAdapter adapter, View parent, View view, EditoriaBean instance, int position) {
                    if(positionSelected == position){
                        view.setBackgroundResource(R.color.menu_3);
                    }else{
                        view.setBackgroundResource(R.drawable.menu_list_background);
                    }
                    Log.d("SporTV", "VIEW "+view.getBackground()+ " "+position);
                }
            });
            mEditoriasListView = (ListView) findViewById(R.id.left_menu);
            //EditoriasDrawerAdapter mAdapter = new EditoriasDrawerAdapter(this, android.R.layout.simple_list_item_1, mEditoriasList);
            mEditoriasListView.setAdapter(mAdapter);
            mEditoriasListView.setOnItemClickListener(new DrawerItemClickListener());
            if (!fragmentAttached) {
                mEditoriasListView.performItemClick(mAdapter.getView(0, null, null), 0, 0);
            }
        }
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    /**
     * Swaps fragments in the main content view
     */
    private void selectItem(int position) {
        positionSelected = position;
        // Create a new fragment and specify the planet to show based on position
        VideosFragment fragment = new VideosFragment();
        Bundle args = new Bundle();
        args.putInt("idEditoria", mEditoriasList.get(position).id);
        args.putString("nomeEditoria", mEditoriasList.get(position).nome);
        fragment.setArguments(args);

        // Insert the fragment by replacing any existing fragment
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.content_frame, fragment, FRAGMENT_VIDEOS_ID)
                .commit();

        // Highlight the selected item, update the title, and close the drawer
        mEditoriasListView.setItemChecked(position, true);
        setTitle(mEditoriasList.get(position).nome);
        mDrawerLayout.closeDrawer(mLeftDrawer);
    }


    public void setTitle(String title) {
        if (title.equalsIgnoreCase(SELECIONE_UM_CAMPEONATO)) {
            getSupportActionBar().setTitle(Util.setColoredString(getApplicationContext(), SELECIONE_UM_CAMPEONATO.toUpperCase()));
        }else{
            this.mTitulo = title;
            getSupportActionBar().setTitle(Util.setColoredString(getApplicationContext(), this.mTitulo.toUpperCase()));
        }
    }

    public PullToRefreshAttacher getPullToRefreshAttacher() {
        return mPullToRefreshAttacher;
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (mEditoriasList != null && mEditoriasList.size() > 0) {
            outState.putParcelableArrayList("listaEditorias", mEditoriasList);
        }
        outState.putInt("positionSelected", positionSelected);
        if (mTitulo != null) {
            outState.putString("mTitulo", mTitulo);
        }
        super.onSaveInstanceState(outState);
    }
}
