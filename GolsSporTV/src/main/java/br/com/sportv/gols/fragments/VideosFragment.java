package br.com.sportv.gols.fragments;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;


import com.loopj.android.http.JsonHttpResponseHandler;
import com.mobsandgeeks.adapters.InstantAdapter;
import com.nhaarman.listviewanimations.appearance.AnimationAdapter;
import com.nhaarman.listviewanimations.appearance.simple.SwingBottomInAnimationAdapter;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import br.com.sportv.gols.HomeActivity;
import br.com.sportv.gols.R;
import br.com.sportv.gols.adapter.VideoAdapter;
import br.com.sportv.gols.http.ApiRestClient;
import br.com.sportv.gols.http.PublicidadeTask;
import br.com.sportv.gols.utils.Constants;
import br.com.sportv.gols.utils.Util;
import br.com.sportv.gols.vo.PublicidadeBean;
import br.com.sportv.gols.vo.VideoBean;
import uk.co.senab.actionbarpulltorefresh.extras.actionbarcompat.PullToRefreshAttacher;


public class VideosFragment extends Fragment implements
        PullToRefreshAttacher.OnRefreshListener {
    View view;
    public int idEditoria;
    ArrayList<VideoBean> mVideoList = new ArrayList<VideoBean>();
    ListView mListView;
    GridView mGridView;
    LinearLayout layoutLoading;
    TextView textViewEmpty;
    FrameLayout layoutContent;
    private PullToRefreshAttacher mPullToRefreshAttacher;
    Context mContext;
    private String nomeEditoria;
    InstantAdapter<VideoBean> adapter;
    JsonHttpResponseHandler responseHandler;
    private ImageView imagePublicidade;

    public static VideosFragment newInstance(String title) {
        return new VideosFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("SporTV", "ON ACTIVITY CREATE");

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (getActivity() != null && mContext == null) {
            mContext = activity.getApplicationContext();
        }


//        if (mVideoList != null && mVideoList.size() > 0) {
//            drawVideos();
//        } else {
//            callServer();
//        }
    }

    private void callServer() {
        ApiRestClient api = new ApiRestClient(getActivity().getApplicationContext());

        responseHandler = new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(JSONObject videosJson) {
                Log.d("SporTV", "RETORNO " + videosJson.toString());

                try {
                    mVideoList = new ArrayList<VideoBean>();
                    if (videosJson.has("count")) {
                        if (videosJson.getInt("count") > 0) {
                            if (videosJson.has("results")) {
                                JSONArray jResults = videosJson.getJSONArray("results");
                                for (int i = 0; i < jResults.length(); i++) {
                                    JSONObject jItem = jResults.getJSONObject(i);
                                    VideoBean v = new VideoBean(jItem);
                                    v.editoria = nomeEditoria;
                                    mVideoList.add(v);
                                }
                            }
                        }
                    }

                    drawVideos();
                } catch (JSONException e) {

                }

                Log.d("SporTV", "RETORNO " + mVideoList.size());
            }

            @Override
            public void onFinish() {
                super.onFinish();
                if (mPullToRefreshAttacher.isRefreshing()) {
                    mPullToRefreshAttacher.setRefreshComplete();
                }
            }
        };
        api.getVIdeos(idEditoria, responseHandler);
    }

    private void drawVideos() {

        if (mContext != null) {
            if (mVideoList.size() > 0) {

//                adapter = new InstantAdapter<VideoBean>(
//                        this.getActivity(), R.layout.videos_listitem, VideoBean.class,
//                        mVideoList);
//
//                adapter.setViewHandler(R.id.imageViewThumbVideo, new ViewHandler<VideoBean>() {
//                    @Override
//                    public void handleView(ListAdapter adapter, View parent, View view, VideoBean video, int position) {
//                        ImageView imgThumb = (ImageView) view.findViewById(R.id.imageViewThumbVideo);
//                        Picasso.with(mContext).load(video.getThumb()).into(imgThumb);
//                        Log.d("SporTV", "load url image " + video.getThumb());
//                    }
//                });

                VideoAdapter adapter = new VideoAdapter(mContext, R.layout.videos_listitem, mVideoList, nomeEditoria);
                if (mListView != null) {
                    AnimationAdapter animAdapter = new SwingBottomInAnimationAdapter(adapter);
                    animAdapter.setAbsListView(mListView);
                    mListView.setAdapter(animAdapter);
                    mListView.setOnItemClickListener(mItemClickListener);
                } else {
                    AnimationAdapter animAdapter = new SwingBottomInAnimationAdapter(adapter);
                    animAdapter.setAbsListView(mGridView);
                    mGridView.setAdapter(animAdapter);
                    mGridView.setOnItemClickListener(mItemClickListener);
                }

                showLoading(false);
                showContent(true);
                showEmptyText(false);
            } else {
                showLoading(false);
                showContent(false);
                showEmptyText(true);
            }
        }

    }

    AdapterView.OnItemClickListener mItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            try {
                VideoBean v = mVideoList.get(position);

                Uri myUrl = Uri.parse(v.midia);
                Intent intentv = new Intent(Intent.ACTION_VIEW);
                intentv.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intentv.setDataAndType(myUrl, "video/*");
                intentv.putExtra(Intent.EXTRA_TITLE, v.titulo);
                mContext.startActivity(intentv);

            } catch (Exception e) {
                Log.e("SporTV", "Erro ao selecionar um video", e);
            }
        }
    };

    @Override
    public void onResume() {
        Log.d("bb", "bb");
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        Log.d("SporTV", "ON CREATE VIEW");
        if (getActivity() != null) {
            mContext = getActivity().getApplicationContext();
        }
        view = inflater.inflate(R.layout.fragment_videos, container, false);

        mListView = (ListView) view.findViewById(R.id.videosFragment_videosListView);
        mGridView = (GridView) view.findViewById(R.id.videosFragment_videosGridView);
        layoutLoading = (LinearLayout) view.findViewById(R.id.videosFragment_progressContainer);
        layoutContent = (FrameLayout) view.findViewById(R.id.videosFragment_contentContainer);
        textViewEmpty = (TextView) view.findViewById(R.id.videosFragment_empty);
        imagePublicidade = (ImageView) view.findViewById(R.id.image_publicidade);


        showLoading(true);
        showContent(false);
        showEmptyText(false);
        // Now get the PullToRefresh attacher from the Activity. An exercise to the reader
        // is to create an implicit interface instead of casting to the concrete Activity
        mPullToRefreshAttacher = ((HomeActivity) getActivity())
                .getPullToRefreshAttacher();
        // Now set the ScrollView as the refreshable view, and the refresh listener (this)
        if (mListView != null) {
            mPullToRefreshAttacher.addRefreshableView(mListView, this);
        } else if (mGridView != null) {
            mPullToRefreshAttacher.addRefreshableView(mGridView, this);
        }
        if (savedInstanceState != null) {

            if (savedInstanceState.containsKey("listaVideos")) {
                mVideoList = savedInstanceState.getParcelableArrayList("listaVideos");
//                drawVideos();
            }
            if (savedInstanceState.containsKey("idEditoria")) {
                idEditoria = savedInstanceState.getInt("idEditoria");
//                callServer();
            }
            if (savedInstanceState.containsKey("nomeEditoria")) {
                nomeEditoria = savedInstanceState.getString("nomeEditoria");
            }

        } else {
            if (getArguments().containsKey("idEditoria")) {
                idEditoria = getArguments().getInt("idEditoria");
//            callServer();
            }

            if (getArguments().containsKey("nomeEditoria")) {
                nomeEditoria = getArguments().getString("nomeEditoria");
            }
        }

        if (mVideoList != null && mVideoList.size() > 0) {
            drawVideos();
        } else {
            callServer();
        }

        Util.initPublicidade(getActivity(), view);
        if (idEditoria == -1 && savedInstanceState == null) {
            criaReceiver(getActivity());
            PublicidadeTask pTask = new PublicidadeTask(getActivity(),
                    Constants.INTENT_PUBLICIDADE, true);
            pTask.execute();
        }
        return view;

    }

    public void showLoading(boolean blnShow) {
        if (blnShow) {
            layoutLoading.setVisibility(View.VISIBLE);
        } else {
            layoutLoading.setVisibility(View.GONE);
        }
    }

    public void showContent(boolean blnShow) {
        if (blnShow) {
            layoutContent.setVisibility(View.VISIBLE);
        } else {
            layoutContent.setVisibility(View.GONE);
        }
    }

    public void showEmptyText(boolean blnShow) {
        if (blnShow) {
            textViewEmpty.setVisibility(View.VISIBLE);
        } else {
            textViewEmpty.setVisibility(View.GONE);
        }
    }

    @Override
    public void onRefreshStarted(View view) {
        callServer();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putInt("idEditoria", idEditoria);
        if (nomeEditoria != null) {
            outState.putString("nomeEditoria", nomeEditoria);
        }
        if (mVideoList != null && mVideoList.size() > 0) {
            outState.putParcelableArrayList("listaVideos", mVideoList);
        }
        super.onSaveInstanceState(outState);

    }

    public void criaReceiver(Activity atividade) {
        IntentFilter iF = new IntentFilter();
        iF.addAction(Constants.INTENT_PUBLICIDADE);
        atividade.registerReceiver(mReceiver, iF);
    }

    public void destroiReceiver(Activity atividade) {
        try {
            atividade.unregisterReceiver(mReceiver);
        } catch (Exception e) {
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        destroiReceiver(getActivity());
    }

    @Override
    public void onPause() {
        Log.d("aa", "aa");
        super.onPause();
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.getAction().equals(Constants.INTENT_PUBLICIDADE)) {
                PublicidadeBean publicidade = (PublicidadeBean) intent
                        .getSerializableExtra("publicidade");
                if (publicidade != null) {
                    Picasso.with(getActivity()).load(publicidade.getImagem()).into(imagePublicidade);
                    Util.criaPublicidade(getActivity(), publicidade);
                }

            }
        }
    };
}