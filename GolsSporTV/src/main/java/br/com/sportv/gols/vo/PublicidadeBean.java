package br.com.sportv.gols.vo;

import java.io.Serializable;

public class PublicidadeBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5771080576801408430L;
	public String link;
	public String imagem;
	
	public PublicidadeBean(String link, String imagem) {
		super();
		this.link = link;
		this.imagem = imagem;
	}

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

	
}
