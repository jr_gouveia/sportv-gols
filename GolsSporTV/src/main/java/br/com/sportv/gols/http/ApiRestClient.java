package br.com.sportv.gols.http;


import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import br.com.sportv.gols.sharedprefs.AppPrefs;

public class ApiRestClient {

    private static final String BASE_URL = "https://api.futebol.globosat.tv";

    private AsyncHttpClient client = new AsyncHttpClient();
    private AppPrefs prefs;
    private Context context;

    public ApiRestClient (Context context){
        client = new AsyncHttpClient();
        prefs = new AppPrefs(context);
        this.context = context;

    }

    public void getToken(AsyncHttpResponseHandler responseHandler) {
        RequestParams params = new RequestParams();
        params.put("username", "panEsportes");
        params.put("password", "p4n3$p0r73$");
        client.post(BASE_URL + "/token/", params, responseHandler);
    }

    public void getEditorias(AsyncHttpResponseHandler responseHandler) {
        if(prefs.contains(context, AppPrefs.TOKEN)){
            client.addHeader("Authorization", "Token "+prefs.getString(context, AppPrefs.TOKEN));
        }
        client.get(BASE_URL + "/editoria/list/?app=32&format=json", null, responseHandler);
        //client.get(BASE_URL + "/futebol/campeonato/list/?app=32&format=json", null, responseHandler);
    }

    public void getVIdeos(int idEditoria, AsyncHttpResponseHandler responseHandler) {
        if(prefs.contains(context, AppPrefs.TOKEN)){
            client.addHeader("Authorization", "Token "+prefs.getString(context, AppPrefs.TOKEN));
        }
        if(idEditoria > -1) {
            client.get(BASE_URL + "/editoria/video/list/?editoria=" + idEditoria + "&format=json&tags=gols", null, responseHandler);
        }else{
            client.get(BASE_URL + "/editoria/video/list/?format=json&tags=gols", null, responseHandler);
        }

    }

    //public static void buscaTime(String nomeTime, int qtd, int offset, AsyncHttpResponseHandler responseHandler) throws UnsupportedEncodingException {
    //    client.get(BASE_URL+"/time/buscar?nome="+ URLEncoder.encode(nomeTime, "UTF-8")+"&rows="+qtd+"&start="+offset, null, responseHandler);
    //}

}
