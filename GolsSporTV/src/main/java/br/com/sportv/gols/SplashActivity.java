package br.com.sportv.gols;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import br.com.sportv.gols.http.ApiRestClient;
import br.com.sportv.gols.http.PublicidadeTask;
import br.com.sportv.gols.utils.Constants;
import br.com.sportv.gols.utils.HttpUtils;
import br.com.sportv.gols.vo.PublicidadeBean;

public class SplashActivity extends Activity implements Runnable {

    boolean startouApp = false;
    int contador = 0;

    boolean temToken = false;
    boolean tokenValido = false;
    View loadingView;
    ImageView splashOferecimento;
    ImageView splashPrincipal;

    boolean carregouPublicidade = false;
    boolean temPublicidade = false;
//    private final ImageDownloader imageDownloader = new ImageDownloader();
//

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

//        getSupportActionBar().hide();

        splashPrincipal = (ImageView) findViewById(R.id.imageSplashTime);
        splashOferecimento = (ImageView) findViewById(R.id.imageSplashOferecimento);
        loadingView = findViewById(R.id.fundoescuro);

        if (HttpUtils.temConexao(getApplicationContext())) {
            criaReceiver();
            loadingView.setVisibility(View.VISIBLE);
            HttpUtils.getToken(getApplicationContext(), new ApiRestClient(getApplicationContext()));

            PublicidadeTask pTask = new PublicidadeTask(this,
                    Constants.INTENT_OFERECIMENTO);
            pTask.execute();
        } else {
            erroConexao();
        }

        Handler h;
        h = new Handler();
        h.postDelayed(SplashActivity.this, 3000);

    }

    @Override
    public void run() {
        if (temToken && carregouPublicidade) {
            if (tokenValido) {
                if (contador < 1 && temPublicidade) {
                    mostraPatrocinadores();
                } else {
                    entraApp();
                }
            } else {
                erroConexao();
            }
        } else {
            Handler h2 = new Handler();
            h2.postDelayed(SplashActivity.this, 1000);
        }
    }

    private void mostraPatrocinadores() {
        contador++;
        Handler h2 = new Handler();
        h2.postDelayed(SplashActivity.this, 3000);


        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
            splashPrincipal.setVisibility(View.GONE);
            splashOferecimento.setVisibility(View.VISIBLE);

        } else {
            // Set the content view to 0% opacity but visible, so that it is visible
            // (but fully transparent) during the animation.
            splashOferecimento.setAlpha(0f);
            splashOferecimento.setVisibility(View.VISIBLE);

            // Animate the content view to 100% opacity, and clear any animation
            // listener set on the view.
            splashOferecimento.animate()
                    .alpha(1f)
                    .setDuration(400)
                    .setListener(null);

            // Animate the loading view to 0% opacity. After the animation ends,
            // set its visibility to GONE as an optimization step (it won't
            // participate in layout passes, etc.)
            splashPrincipal.animate()
                    .alpha(0f)
                    .setDuration(400)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            splashPrincipal.setVisibility(View.GONE);
                        }
                    })
            ;

        }

    }

    private void entraApp() {
        if (!startouApp) {
            Intent i = new Intent(this, HomeActivity.class);

            i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            startActivity(i);
            destroiReceiver();

            startouApp = true;
            finish();

        }
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            if (intent.hasExtra(Constants.TOKEN_RECEIVED)) {
                if (loadingView != null) {
                    loadingView.setVisibility(View.GONE);
                }
                temToken = true;
                tokenValido = intent.getBooleanExtra(Constants.TOKEN_RECEIVED, false);
            } else if (intent.getAction().equals(Constants.INTENT_OFERECIMENTO)) {
                PublicidadeBean publicidade = (PublicidadeBean) intent
                        .getSerializableExtra("publicidade");
                carregouPublicidade = true;
                if (publicidade != null) {
                    temPublicidade = true;
                    Picasso.with(SplashActivity.this).load(publicidade.getImagem()).into(splashOferecimento);
                }else{
                    temPublicidade = false;
                }

            }
        }
    };

    public void criaReceiver() {
        IntentFilter iF = new IntentFilter();
        iF.addAction(Constants.INTENT_TOKEN);
        iF.addAction(Constants.INTENT_OFERECIMENTO);
        registerReceiver(mReceiver, iF);
    }

    public void destroiReceiver() {
        try {
            unregisterReceiver(mReceiver);
        } catch (Exception e) {
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        destroiReceiver();
    }

    private void erroConexao() {
        Toast.makeText(this, "falta de conexão", Toast.LENGTH_SHORT).show();
        finish();
    }
}
