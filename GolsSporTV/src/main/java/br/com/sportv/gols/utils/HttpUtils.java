package br.com.sportv.gols.utils;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;

import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONObject;

import br.com.sportv.gols.http.ApiRestClient;
import br.com.sportv.gols.sharedprefs.AppPrefs;

public class HttpUtils {

    public static void getToken(final Context context, ApiRestClient api) {

        api.getToken(new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(JSONObject jsonObject) {
                super.onSuccess(jsonObject);
                Intent i = new Intent(Constants.INTENT_TOKEN);
                if (jsonObject.has("token")) {
                    String newToken = jsonObject.optString("token");
                    AppPrefs prefs = new AppPrefs(context);
                    if(prefs.contains(context, AppPrefs.TOKEN)){
                        prefs.remove(context, AppPrefs.TOKEN);
                    }
                    prefs.putString(context, AppPrefs.TOKEN, newToken);
                    i.putExtra(Constants.TOKEN_RECEIVED, true);
                } else {
                    i.putExtra(Constants.TOKEN_RECEIVED, false);
                }
                context.sendBroadcast(i);
            }

            @Override
            public void onFailure(Throwable throwable, String s) {
                super.onFailure(throwable, s);
                Intent i = new Intent(Constants.INTENT_TOKEN);
                i.putExtra(Constants.TOKEN_RECEIVED, false);
                context.sendBroadcast(i);
            }

            @Override
            public void onFailure(Throwable throwable, JSONObject jsonObject) {
                super.onFailure(throwable, jsonObject);
                Intent i = new Intent(Constants.INTENT_TOKEN);
                i.putExtra(Constants.TOKEN_RECEIVED, false);
                context.sendBroadcast(i);
            }

            @Override
            public void onFailure(Throwable throwable, JSONArray jsonArray) {
                super.onFailure(throwable, jsonArray);
                Intent i = new Intent(Constants.INTENT_TOKEN);
                i.putExtra(Constants.TOKEN_RECEIVED, false);
                context.sendBroadcast(i);
            }

            @Override
            public void onFailure(Throwable throwable) {
                super.onFailure(throwable);
                Intent i = new Intent(Constants.INTENT_TOKEN);
                i.putExtra(Constants.TOKEN_RECEIVED, false);
                context.sendBroadcast(i);
            }
        });

    }

    public static boolean temConexao(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getActiveNetworkInfo() != null
                && connectivityManager.getActiveNetworkInfo().isConnected()) {
            return true;
        }
        return false;
    }
}
