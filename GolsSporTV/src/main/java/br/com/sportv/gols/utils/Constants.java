package br.com.sportv.gols.utils;

public class Constants {

    public static final String INTENT_TOKEN = "br.com.sportv.gols.token";

    public static final String INTENT_OFERECIMENTO = "br.com.sportv.gols.oferecimento";
    public static final String INTENT_PUBLICIDADE = "br.com.sportv.gols.publicidade";

    public static final String TOKEN_RECEIVED = "tokenreceived";

}
