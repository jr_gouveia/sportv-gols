package br.com.sportv.gols.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.net.Uri;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.nineoldandroids.animation.Animator;
import com.nineoldandroids.animation.Animator.AnimatorListener;
import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import br.com.sportv.gols.HomeActivity;
import br.com.sportv.gols.R;
import br.com.sportv.gols.vo.PublicidadeBean;

public class Util {

    public static Map<String, Typeface> typefaceCache = new HashMap<String, Typeface>();

    public static String calendarToString(Calendar cal) {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(cal.getTime());
    }

    public static float converteDPparaPX(float dp, Context context) {
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (float) dp, context.getResources()
                .getDisplayMetrics());
        return px;
    }

    public static void initPublicidade(Activity atividade, View rootView) {
        try {
            final View linearPublicidade = rootView.findViewById(R.id.root_publicidade);
            final int slide = (int) converteDPparaPX(95, atividade.getApplicationContext());
            if(linearPublicidade != null) {
                ObjectAnimator.ofFloat(linearPublicidade, "translationY", slide).setDuration(0).start();
            }
        } catch (Exception e) {
            Log.e("Animação", "Erro na animação", e);
        }
    }

    // metodo que vai montar dinamicamente a propaganda

    public static void criaPublicidade(final Activity atividade, PublicidadeBean publicidade) {
        if (atividade != null) {
            final View linearPublicidade = atividade.findViewById(R.id.root_publicidade);
            if (linearPublicidade != null) {
                final int slide = (int) converteDPparaPX(95, atividade.getApplicationContext());
                final int duration = 700;
                ImageView imgPublicidade = (ImageView) linearPublicidade.findViewById(R.id.image_publicidade);
                final LinearLayout linearFecharPublicidade = (LinearLayout) atividade
                        .findViewById(R.id.linear_root_fechar_publicidade);
                try {
                    final AnimatorSet set = new AnimatorSet();
                    set.playSequentially(
                            ObjectAnimator.ofFloat(linearPublicidade, "translationY", slide, 0)
                                    .setDuration(duration),
                            ObjectAnimator.ofFloat(linearPublicidade, "alpha", 1, 1).setDuration(4 * 1000),
                            ObjectAnimator.ofFloat(linearPublicidade, "translationY", slide).setDuration(
                                    duration));
                    set.setStartDelay(1000);
                    set.start();

                    set.addListener(new Animator.AnimatorListener() {

                        @Override
                        public void onAnimationStart(Animator animation) {
                            linearPublicidade.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onAnimationRepeat(Animator animation) {
                        }

                        @Override
                        public void onAnimationEnd(Animator animation) {
                            linearPublicidade.clearAnimation();
                            linearPublicidade.invalidate();
                            linearPublicidade.refreshDrawableState();
                            linearPublicidade.setVisibility(View.GONE);
                            linearPublicidade.setClickable(false);
                        }

                        @Override
                        public void onAnimationCancel(Animator animation) { //
                            linearPublicidade.setClickable(false);
                            Toast.makeText(atividade, "CANCEL", Toast.LENGTH_SHORT).show();
                        }
                    });
                    linearFecharPublicidade.setOnClickListener(new OnClickListener() {

                        @Override
                        public void onClick(View v) {
                            set.removeAllListeners();
                            set.cancel();
                            ObjectAnimator oa = ObjectAnimator.ofFloat(linearPublicidade, "translationY",
                                    slide).setDuration(duration);
                            oa.start();

                            oa.addListener(new AnimatorListener() {

                                @Override
                                public void onAnimationStart(Animator animation) {
                                    linearPublicidade.setVisibility(View.VISIBLE);
                                }

                                @Override
                                public void onAnimationRepeat(Animator animation) {
                                }

                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    linearPublicidade.clearAnimation();
                                    linearPublicidade.invalidate();
                                    linearPublicidade.refreshDrawableState();
                                    linearPublicidade.setVisibility(View.GONE);
                                    linearPublicidade.setClickable(false);
                                }

                                @Override
                                public void onAnimationCancel(Animator animation) {
                                }
                            });
                        }
                    });


                    final String strLink = publicidade.link;
                    if (strLink != null && strLink.length() > 0) {
                        imgPublicidade.setOnClickListener(new View.OnClickListener() {

                            @Override
                            public void onClick(View v) {

                                try {
                                    Intent i = new Intent(Intent.ACTION_VIEW);
                                    i.setData(Uri.parse(strLink));
                                    atividade.startActivity(i);
                                } catch (Exception e) {
                                }

                            }
                        });
                    }

//                    }
                } catch (Exception e) {
                    //Util.log("Erro no download da publicidade", e);
                }
            }
        }
    }

    public static boolean collapse(LinearLayout l) {
        if (l != null) {

            AlphaAnimation aa = new AlphaAnimation(1, 0);
            aa.setDuration(500);
            aa.setInterpolator(new AccelerateDecelerateInterpolator());
            l.startAnimation(aa);
            aa.setFillAfter(true);
            l.setVisibility(View.GONE);
            // retorna false pq o loading não está aberto
            return false;
        }
        // retorna true pq o loading continua aberto
        return true;
    }

    public static boolean expand(LinearLayout l) {
        if (l != null) {
            AlphaAnimation aa = new AlphaAnimation(0, 1);
            aa.setDuration(500);
            aa.setInterpolator(new AccelerateDecelerateInterpolator());
            l.startAnimation(aa);
            aa.setFillAfter(true);
            l.setVisibility(View.VISIBLE);
            return true;
        }
        return false;
    }

    public static SpannableStringBuilder setColoredString(Context context, String mText) {
        final SpannableStringBuilder sb = new SpannableStringBuilder(mText);
        final ForegroundColorSpan fcs1 = new ForegroundColorSpan(context.getResources().getColor(R.color.sportv_azul));
        final ForegroundColorSpan fcs2 = new ForegroundColorSpan(context.getResources().getColor(R.color.sportv_azul));
//        final ForegroundColorSpan fcs2 = new ForegroundColorSpan(context.getResources().getColor(R.color.sportv_vermelho));

        Typeface robotoBold = Typeface.createFromAsset(context.getAssets(),
                "fonts/RobotoCondensed-Bold.ttf");
        Typeface robotoRegular = Typeface.createFromAsset(context.getAssets(),
                "fonts/RobotoCondensed-Regular.ttf");

        int limit1 = mText.indexOf(" ");

        if(limit1 == -1){
            limit1 = mText.length();
        }

        // Span to set text color to some RGB value
        //final StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);

        // Span to make text first color
        sb.setSpan(fcs1, 0, limit1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        // Span to make text second color
        sb.setSpan(fcs2, limit1, mText.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        // Set the text bold
        //sb.setSpan(bss, limit1, mText.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);

        // Set text font regular
        //sb.setSpan(robotoRegular, 0, limit1, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
//        sb.setSpan(new CustomTypefaceSpan("", robotoBold), 0, limit1, Spanned.SPAN_INCLUSIVE_INCLUSIVE);
        sb.setSpan(new CustomTypefaceSpan("", robotoRegular), 0, limit1, Spanned.SPAN_INCLUSIVE_INCLUSIVE);

        if(mText.equalsIgnoreCase(HomeActivity.SELECIONE_UM_CAMPEONATO)){
            sb.setSpan(new CustomTypefaceSpan("", robotoRegular), limit1, mText.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        }else{
            //Set text font bold
            sb.setSpan(new CustomTypefaceSpan("", robotoBold), limit1, mText.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        }


        return sb;

    }



    public static void setTypeface(AttributeSet attrs, TextView textView) {
        Context context = textView.getContext();

        TypedArray values = context.obtainStyledAttributes(attrs, R.styleable.CustomTextView);
        String typefaceName = values.getString(R.styleable.CustomTextView_typeface);

        if (typefaceCache.containsKey(typefaceName)) {
            textView.setTypeface(typefaceCache.get(typefaceName));
        } else {
            Typeface typeface;
            try {
                typeface = Typeface.createFromAsset(textView.getContext().getAssets(), "fonts/" + typefaceName);
            } catch (Exception e) {
                Log.v(context.getString(R.string.app_name), String.format("fonte não encontrada", typefaceName));
                return;
            }

            typefaceCache.put(typefaceName, typeface);
            textView.setTypeface(typeface);
        }
    }
}
