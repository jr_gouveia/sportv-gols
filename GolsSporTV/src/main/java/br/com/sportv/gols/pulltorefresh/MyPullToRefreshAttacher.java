package br.com.sportv.gols.pulltorefresh;

import android.app.Activity;
import android.content.Context;
import android.support.v7.app.ActionBarActivity;

import uk.co.senab.actionbarpulltorefresh.library.PullToRefreshAttacher;

public class MyPullToRefreshAttacher extends PullToRefreshAttacher {
    protected MyPullToRefreshAttacher(Activity activity, Options options) {
        super(activity, options);
    }

    public static MyPullToRefreshAttacher get(Activity activity) {
        return get(activity, new Options());
    }

    public static MyPullToRefreshAttacher get(Activity activity, Options options) {
        return new MyPullToRefreshAttacher(activity, options);
    }

    @Override
    protected HeaderTransformer createDefaultHeaderTransformer() {
        return new MyHeaderTransformer();
    }

    @Override
    protected EnvironmentDelegate createDefaultEnvironmentDelegate() {
        return new MyEnvironmentDelegate();
    }

    public static class MyEnvironmentDelegate extends EnvironmentDelegate {
        /**
         * @return Context which should be used for inflating the header layout
         */
        public Context getContextForInflater(Activity activity) {
            if (activity instanceof ActionBarActivity) {
                return ((ActionBarActivity) activity).getSupportActionBar().getThemedContext();
            }
            return super.getContextForInflater(activity);
        }
    }
}
