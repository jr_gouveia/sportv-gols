package br.com.sportv.gols.vo;

import android.os.Parcel;
import android.os.Parcelable;

import com.mobsandgeeks.adapters.InstantText;

import org.json.JSONException;
import org.json.JSONObject;

import br.com.sportv.gols.R;

public class EditoriaBean  implements Parcelable{

    public int id;
    public int idFootstats;
    public int idGe;
    public String nome;
    public int rodadaAtual;
    public int rodadas;

    public EditoriaBean(){

    }

    public EditoriaBean(JSONObject json) throws JSONException {
        if (json.has("id") && !json.isNull("id")) {
            id = json.getInt("id");
        }

        if (json.has("id_footstats") && !json.isNull("id_footstats")) {
            idFootstats = json.getInt("id_footstats");
        }

        if (json.has("id_ge") && !json.isNull("id_ge")) {
            idGe = json.getInt("id_ge");
        }

        if (json.has("nome")) {
            nome = json.getString("nome");
        }

        if (json.has("rodada_atual") && !json.isNull("rodada_atual")) {
            rodadaAtual = json.getInt("rodada_atual");
        }

        if (json.has("rodadas") && !json.isNull("rodadas")) {
            rodadas = json.getInt("rodadas");
        }

    }

    @InstantText(viewId = R.id.menuItem_titulo)
    public String getNome() {
        return nome;
    }

    protected EditoriaBean(Parcel in) {
        id = in.readInt();
        idFootstats = in.readInt();
        idGe = in.readInt();
        nome = in.readString();
        rodadaAtual = in.readInt();
        rodadas = in.readInt();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(idFootstats);
        dest.writeInt(idGe);
        dest.writeString(nome);
        dest.writeInt(rodadaAtual);
        dest.writeInt(rodadas);
    }

    public static final Parcelable.Creator<EditoriaBean> CREATOR = new Parcelable.Creator<EditoriaBean>() {
        public EditoriaBean createFromParcel(Parcel in) {
            return new EditoriaBean(in);
        }

        public EditoriaBean[] newArray(int size) {
            return new EditoriaBean[size];
        }
    };

}
