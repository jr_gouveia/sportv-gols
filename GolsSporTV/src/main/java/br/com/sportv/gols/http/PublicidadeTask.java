package br.com.sportv.gols.http;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import net.htmlparser.jericho.Element;
import net.htmlparser.jericho.HTMLElementName;
import net.htmlparser.jericho.Source;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import br.com.sportv.gols.vo.PublicidadeBean;

public class PublicidadeTask extends AsyncTask<Integer, Void, String> {

    Activity atividade;
    View view;
    String url = "http://ads.globo.com/RealMedia/ads/adstream_sx.ads/oggsatmobile/android/sportv/gols/600x1024@x02!x02";
//    String url = "http://ads.globo.com/RealMedia/ads/adstream_sx.ads/oggsatmobile/iphone/sportv/gols@x02!x02";
    String url2 = "http://ads.globo.com/RealMedia/ads/adstream_sx.ads/oggsatmobile/android/sportv/gols@x01!x01";
    URL urlNoticia;
    StringBuffer strHTML = new StringBuffer(1024);
    boolean erroConexao = false;
    String strIntent = "";
    PublicidadeBean publicidade = null;
    boolean isBanner = false;

    public PublicidadeTask(Activity mActivity, String strIntent) {
        super();
        this.atividade = mActivity;
        this.strIntent = strIntent;
    }

    public PublicidadeTask(Activity mActivity, String strIntent, boolean isBanner) {
        super();
        this.atividade = mActivity;
        this.strIntent = strIntent;
        this.isBanner = isBanner;
    }

    @Override
    protected String doInBackground(Integer... params) {
        return buscaPublicidade();

    }

    @Override
    protected void onPostExecute(String str) {
        Intent intent = new Intent(strIntent);
        if (publicidade != null) {
            intent.putExtra("publicidade", publicidade);
        }
        intent.putExtra("erroConexao", erroConexao);
        atividade.sendBroadcast(intent);
        super.onPostExecute(str);
    }

    protected String buscaPublicidade() {
        ConnectivityManager connectivityManager = (ConnectivityManager) atividade
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getActiveNetworkInfo() != null
                && connectivityManager.getActiveNetworkInfo().isConnected()) {

            try {

                // busca a lista de jogos do campeonato
                if (isBanner) {
                    urlNoticia = new URL(url2);
                } else {
                    urlNoticia = new URL(url);
                }
                BufferedReader in;
                in = new BufferedReader(new InputStreamReader(
                        urlNoticia.openStream()));
                String str;

                while ((str = in.readLine()) != null) {
                    strHTML.append(str.trim());
                }
                in.close();
                Source s = new Source(strHTML.toString());

                // busca da versao inteira
                // Element e = s.getElementById("materia-letra");
                // String strTemp = e.toString();

                // busca da versao mobile
                String strA = "";
                String strImagem = "";
                // trata para retirar as tags não necessarias
                List<Element> lstA = s.getAllElements(HTMLElementName.A);
                for (Element element : lstA) {
                    strA = element.getAttributeValue("href");
                    List<Element> lstIMG = element.getAllElements(HTMLElementName.IMG);
                    for (Element elementIMG : lstIMG) {
                        strImagem = elementIMG.getAttributeValue("src");
                    }
                }
                if (strImagem != "") {
                    publicidade = new PublicidadeBean(strA, strImagem);
                } else {
                    publicidade = null;
                }

                return null;
            } catch (MalformedURLException e) {
                Log.e("PublicidadeTask", "URL INCORRETA", e);
            } catch (IOException e) {
                Log.e("PublicidadeTask", "ERRO DE I/O", e);
            } catch (Exception e) {
                Log.e("PublicidadeTask", "ERRO NAO IDENTIFICADO", e);
            }
        } else {
            erroConexao = true;
        }
        return null;
    }


}
